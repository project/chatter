Drupal.behaviors.chatter = function (context) {
  $("#chatter input[id*=edit-comment]:not(.chatter-processed)", context).addClass('chatter-processed').click(function(){
    if ($(this).attr('value') == 'Type your comment here.')  {
      $(this).attr('value', '');
    }
  });
  $("#chatter input[id*=edit-comment].chatter-processed").blur(function(){
    if ($(this).attr('value') == '')  {
      $(this).attr('value', 'Type your comment here.');
    }
  });
  $("#chatter input#edit-submit[id*=edit-comment]:not(.chatter-processed)").addClass('chatter-processed').click(function() {
    $("#edit-comment").val("Type your comment here.");
  });
}