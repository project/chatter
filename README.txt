
/**
 * @file
 * README.txt file for Chatter Box module.
 */

Description
The Chatter Box module provides a simple node-based chat system. A block is provided with
a simplified comment reply form and a list of comments. Each chat post is actually a comment on a node.

Configurable features
 - Selection of content type to be used as chat nodes.
 - Number of comments in the list.
 - Truncation of comments to specified length
 - Auto creation of new nodes (with Cronplus module).
 - Set default title and body for new nodes. Date can be inserted.
 - Select date format for use in default text.
 - Set comments to read only on the closed node.

Benefits
 - Allows placement of chat input in any theme region.
 - Automatic and self-maintaining.
 - Chats are archived and indexed for searching.
 - Chat posts may be administered like regular comments, with permissions, moderation, editing, deletion.
 - Compatible with CAPTCHA modules, although they detract from the chat-like nature of Chatter Box.

Suggestions
 - This module intends for the Chatter block to be placed on the front page.
 - Note that if you also place the block on the node page, the node's comment form will also be simplified.
 - With the Block Refresh module, new comments by other users appear automatically in the block.
 - Spam prevention modules may affect the input form.
 - You may want to create a new content type just for this module.

Installation
 - Copy the extracted module into your modules directory, usually sites/all/modules.
 - Enable the module.
 - Set the configurable options in the module's settings page.
 - Posistion the block on your blocks page. IMPORTANT: Use "Show on only the listed pages." for your block settings.
   The module will not recognize wildcards.
 - Note that the block will only appear on nodes of the content type you select in the module's settings and on the front
   page if you choose to show the block there.

Theming
 - Theming may be accomplished by adding to your theme's css file. A stylesheet is included with the module called
   chatter.css.
 - The following IDs and classes are provided:
      #chatter - encloses everything in the block.
      #chatter-link - is a link to the current chatter node.
      ul.chatter-list - is the list of chatter comments in the block.
      li.chatter-comment - is each individual comment in the block.

Additional
 - Chatter comments use the site's default input filter.
 - You may want to disable your captcha module from the chatter block on the front page. You will probably have to hack
   your captcha module to do this.
 - For example, to keep captcha.module from the front page, change the first line
   of the function captcha_form_alter($form_id, &$form)
   from:   if (!user_access('skip CAPTCHA') {
   to:     if (!user_access('skip CAPTCHA') && !drupal_is_front_page()) {

Author
awolfey
aaronewolfe@gmail.com